﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class score : MonoBehaviour {
    public Text text;
    int Score;
    // Use this for initialization
    void Start() {
        
    }

    public void AddPoint(int point) {
        Score += point;
    }

    // Update is called once per frame
    void Update() {
        text.text = "score: " + Score;
    }
}