﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class increase : MonoBehaviour {
    public GameObject prefab;
    private float timer = 0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        timer += Time.deltaTime;

        if (timer >= 1f) {
            float x = Random.Range(-7, 7);
            float y = 2;
            float z = -1;
            var position = new Vector3(x, y, z);

            Instantiate(prefab, position, Quaternion.identity);
            timer -= 1f;
        }
	}
}
