﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePolice : MonoBehaviour {
    [SerializeField] float movePower;
    [SerializeField] float police_PP;
    [SerializeField] int move = 0;
    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            this.transform.Translate(transform.right * movePower);

        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            this.transform.Translate(transform.right * -1 * movePower);

        }
        if (Input.GetKeyDown(KeyCode.Space)) {
            police_PP--;
            var rigid = GetComponent<Rigidbody2D>();
            rigid.AddForce(new Vector2(0, 300));
        }

        if (FindObjectOfType<death>().triggerCount == 6) {
            Destroy(this.gameObject);
        }
    }
}
