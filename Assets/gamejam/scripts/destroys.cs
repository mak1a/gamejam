﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroys : MonoBehaviour {
    [SerializeField] float destroyTimer = 5f;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        destroyTimer -= Time.deltaTime;

        if (destroyTimer < 0f) {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("police")) {
            Destroy(gameObject);
            FindObjectOfType<score>().AddPoint(10);
        }
    }
}
